﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;


namespace SemesterProj2
{
    public partial class Form1 : Form
    {
        const int RADIUS = 25;
        const int MARGIN_LEFT = 60;
        const int MARGIN_TOP = 160;
        const int D_X_CALENDAR_GRID = 60;
        const int D_Y_CALENDAR_GRID = 50;
        const string DEFAULT_TITLE = "Calendar";
        const int SIDE_MARGIN_LEFT = 30;
        const int SIDE_MARGIN_TOP = 0;
        const int SIDE_MARGIN_TOP_ADDING = 0;

        // Somthing selected
        int noteIDSelected = -1;
        DateTime dateSelected = DateTime.Now;

        // SIDE BAR VARIABLES
        Panel sideBar;
        int sumHeight;


        Calendar calendar = new Calendar();
        
        public Form1()
        {
            calendar.changed += Invalidate;
            calendar.ReadFromDefault();
            calendar.changed += calendar.WriteInDefault;

            Text = DEFAULT_TITLE;
            ClientSize = new Size(800, 500);
            StartPosition = FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.BackColor = Color.WhiteSmoke;

            sideBar = new Panel();
            sideBar.Location = new Point(MARGIN_LEFT*2 + D_X_CALENDAR_GRID*6, 45+SIDE_MARGIN_TOP);
            sideBar.Size = new Size(320, 440-SIDE_MARGIN_TOP);
            sideBar.AutoScroll = true;
            this.Controls.Add(sideBar);

            CreateSideBar();

            calendar.changed += RefreshSidebar;
        }

        // Helps to detect, which date on the grid has been clicked
        int getRelativeDate (Point mousePos) {
            int d_x = D_X_CALENDAR_GRID;
            int d_y = D_Y_CALENDAR_GRID;

            int i = (mousePos.X - MARGIN_LEFT + d_x / 2) / d_x;
            int j = (mousePos.Y - MARGIN_TOP + d_y / 2) / d_y;

            return j * 7 + i;
        }

        // Handle all clicks
        protected override void OnMouseDown(MouseEventArgs args) {
            Point m_pos = args.Location;
            int d_x = D_X_CALENDAR_GRID;
            int d_y = D_Y_CALENDAR_GRID;

            if (m_pos.X >= MARGIN_LEFT - d_x / 2 && m_pos.Y >= MARGIN_TOP - d_y / 2 && m_pos.X <= MARGIN_LEFT + d_x * 7 - d_x / 2 && m_pos.Y <= MARGIN_TOP + d_y * 6 - d_y / 2) {
                DateTime startFrom = Program.GetStart(dateSelected);
                dateSelected = startFrom.AddDays(getRelativeDate(m_pos));
                noteIDSelected = -1;
                RefreshSidebar();
                Invalidate();
                return;
            }

            if (m_pos.Y >= MARGIN_TOP + d_y * 6 - d_y / 2 && m_pos.Y <= MARGIN_TOP + d_y * 6 + d_y / 2) {
                if (m_pos.X >= MARGIN_LEFT + d_x * 2 && m_pos.X <= MARGIN_LEFT + d_x * 3) {
                    dateSelected = dateSelected.AddMonths(-1);
                    noteIDSelected = -1;
                    RefreshSidebar();
                    Invalidate();
                    return;
                }
                if (m_pos.X >= MARGIN_LEFT + d_x * 3 && m_pos.X <= MARGIN_LEFT + d_x * 4) {
                    dateSelected = dateSelected.AddMonths(1);
                    noteIDSelected = -1;
                    RefreshSidebar();
                    Invalidate();
                    return;
                }
            }

            if (m_pos.X >= 510 && m_pos.Y >= 15+SIDE_MARGIN_TOP_ADDING && m_pos.X <= 510+24 && m_pos.Y <= 15+24+SIDE_MARGIN_TOP_ADDING) {
                noteIDSelected = -1;
                Invalidate();
                RefreshSidebar();
                CreateDialogAdd();
                return;
            }

            if (m_pos.X >= MARGIN_LEFT*2 + D_X_CALENDAR_GRID*6) {
                noteIDSelected = -1;
                Invalidate();
                RefreshSidebar();
                return;
            }
        }

        void UpdateAfterDateChange () {
            noteIDSelected = -1;
            Invalidate();
            RefreshSidebar();
        }

        protected override void OnKeyDown(KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete && noteIDSelected >= 0) {
                calendar.RemoveNote(dateSelected.Date, noteIDSelected);
                UpdateAfterDateChange();
            }
            switch (e.KeyCode) {
            case Keys.Up:
                if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                    dateSelected = dateSelected.AddYears(-1);
                    UpdateAfterDateChange();
                }
                if ((Control.ModifierKeys & Keys.Control) == Keys.Control) {
                    dateSelected = dateSelected.AddDays(-7);
                    UpdateAfterDateChange();
                }
                break;
            case Keys.Down:
                if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                    dateSelected = dateSelected.AddYears(1);
                    UpdateAfterDateChange();
                }
                if ((Control.ModifierKeys & Keys.Control) == Keys.Control) {
                    dateSelected = dateSelected.AddDays(7);
                    UpdateAfterDateChange();
                }
                break;
            case Keys.Left:
                if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                    dateSelected = dateSelected.AddMonths(-1);
                    UpdateAfterDateChange();
                }
                if ((Control.ModifierKeys & Keys.Control) == Keys.Control) {
                    dateSelected = dateSelected.AddDays(-1);
                    UpdateAfterDateChange();
                }
                break;
            case Keys.Right:
                if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                    dateSelected = dateSelected.AddMonths(1);
                    UpdateAfterDateChange();
                }
                if ((Control.ModifierKeys & Keys.Control) == Keys.Control) {
                    dateSelected = dateSelected.AddDays(1);
                    UpdateAfterDateChange();
                }
                break;
            }
        }

        // Computes the length of the string (helps to fit the string into the label)
        int GetLengthInPixels (Font f, string s) {
            using (Bitmap tempImage = new Bitmap(400,400)) {
                SizeF stringSize = Graphics.FromImage(tempImage).MeasureString(s , f);
                return (int)stringSize.Width;
            }
        }

        // FONTS
        Font font = new Font ("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        Font fontTitle = new Font ("Open Sans", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        Font fontBold = new Font ("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        Font fontSmall = new Font ("Open Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        Font font10B = new Font ("Open Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));

        // Dialog box "Add a note"
        void CreateDialogAdd_lblTag (Panel addNotePanel, int d_x, int d_y) {
            Label lblTag = new Label();
            lblTag.Location = new Point(0, 0);
            lblTag.Size = new Size(d_x, d_y);
            lblTag.Text = "Tag:";
            lblTag.Font = fontSmall;
            addNotePanel.Controls.Add(lblTag);
        }

        void CreateDialogAdd_lblName (Panel addNotePanel, int d_x, int d_y) {
            Label lblName = new Label();
            lblName.Location = new Point(0, d_y);
            lblName.Size = new Size(d_x, d_y);
            lblName.Text = "Name:";
            lblName.Font = fontSmall;
            addNotePanel.Controls.Add(lblName);
        }

        void CreateDialogAdd_lblDescr (Panel addNotePanel, int d_x, int d_y) {
            Label lblDescr = new Label();
            lblDescr.Location = new Point(0, d_y*2);
            lblDescr.Size = new Size(d_x, d_y);
            lblDescr.Text = "Description:";
            lblDescr.Font = fontSmall;
            addNotePanel.Controls.Add(lblDescr);
        }

        void CreateDialogAdd (string Title = "Add a note", string Tag = "TODO", string Name = "", string Descr = "", int id = -1) {
            Form dlg = new Form();
            dlg.Text = Title;
            dlg.ClientSize = new Size(360, 200);
            dlg.StartPosition = FormStartPosition.CenterScreen;
            dlg.MaximizeBox = false;
            dlg.MinimizeBox = false;
            dlg.FormBorderStyle = FormBorderStyle.FixedDialog;

            const int D_X = 80;
            const int D_Y = 28;
            const int MARGIN = 14;

            Panel addNotePanel = new Panel();
            addNotePanel.Location = new Point(MARGIN, MARGIN);
            addNotePanel.Size = new Size(360-MARGIN*2, 200-MARGIN*2);
            addNotePanel.AutoScroll = true;
            dlg.Controls.Add(addNotePanel);

            CreateDialogAdd_lblTag(addNotePanel, D_X, D_Y);
            CreateDialogAdd_lblName(addNotePanel, D_X, D_Y);
            CreateDialogAdd_lblDescr(addNotePanel, D_X, D_Y);
            
            TextBox txtTag = new TextBox();
            txtTag.Location = new Point(0 + D_X, 0);
            txtTag.Size = new Size(D_X*3, D_Y);
            txtTag.Font = fontSmall;
            txtTag.Text = Tag;
            addNotePanel.Controls.Add(txtTag);

            TextBox txtName = new TextBox();
            txtName.Location = new Point(0 + D_X, D_Y);
            txtName.Size = new Size(D_X*3, D_Y);
            txtName.Font = fontSmall;
            txtName.Text = Name;
            addNotePanel.Controls.Add(txtName);

            TextBox txtDescr = new TextBox();
            txtDescr.Location = new Point(0 + D_X, D_Y*2);
            txtDescr.Size = new Size(D_X*3, D_X);
            txtDescr.Multiline = true;
            txtDescr.Font = fontSmall;
            txtDescr.Text = Descr;
            addNotePanel.Controls.Add(txtDescr);

            Button btnAdd = new Button();
            btnAdd.Location = new Point(D_X*3, D_Y*5);
            btnAdd.Size = new Size(D_X, D_Y);
            btnAdd.Text = "OK";
            btnAdd.Font = fontSmall;
            addNotePanel.Controls.Add(btnAdd);

            btnAdd.Click += btnAdd_Click;

            void btnAdd_Click (object sender, EventArgs e) {
                if ((txtTag.Text != "" && txtDescr.Text == "") || txtName.Text != "") {
                    if (id != -1) {
                        calendar.RemoveNote(dateSelected.Date, id);
                    }
                    calendar.AddNote(txtTag.Text, dateSelected.Date, dateSelected.Date, txtName.Text, txtDescr.Text);
                    noteIDSelected = calendar.Length(dateSelected.Date);
                    RefreshSidebar();
                    Invalidate();
                    dlg.Close();
                }
            }

            dlg.ShowDialog();
        }

        // Sidebar with tags and notes
        void RefreshSidebar () {
            sideBar.Controls.Clear();
            CreateSideBar();
        }

        int MinInt (int a, int b) {
            return (a>b) ? b : a;
        }

        int CreateSideBar_displayTag (int leftShift, int tagWidth, int tagHeight, Note note) {
            Label lblTag = new Label();
            lblTag.Location = new Point(SIDE_MARGIN_LEFT+leftShift, 5+sumHeight);
            lblTag.Size = new Size(MinInt(tagWidth+7, 240-4), tagHeight);
            if (noteIDSelected == note.ID || calendar.GetTag(dateSelected.Date, noteIDSelected) == note.Tag) {
                lblTag.Size = new Size(MinInt(tagWidth+7, 240-44*2-5), tagHeight);
                lblTag.BackColor = System.Drawing.Color.Green;
                lblTag.ForeColor = System.Drawing.Color.White;
            }
            else
                lblTag.BorderStyle = BorderStyle.FixedSingle;
            lblTag.TextAlign = ContentAlignment.MiddleCenter;
            lblTag.Text = note.Tag;
            lblTag.Font = font;
            sideBar.Controls.Add(lblTag);
            lblTag.Click += selectNote;

            void selectNote(object sender, EventArgs e) {
                noteIDSelected = note.ID;
                Invalidate();
                RefreshSidebar();
            }

            return lblTag.Size.Width;
        }

        int CreateSideBar_contentTags (int i, int notesNum, int lastTagLength, int leftShift, List<string> usedTags) {
            foreach (Note note in calendar.GetNotes(dateSelected.Date)) {
                if (note.Tag.Length == 0 || usedTags.Contains(note.Tag))
                    continue;

                usedTags.Add(note.Tag);

                int tagWidth = GetLengthInPixels(font, note.Tag);
                const int tagHeight = 25;
                const int tagBetween = 4;

                if (leftShift + tagWidth > 240) {
                    sumHeight += tagHeight + tagBetween;
                    leftShift = 0;
                }
                else
                    leftShift += lastTagLength;

                int w = CreateSideBar_displayTag(leftShift, tagWidth, tagHeight, note);
                lastTagLength = w + tagBetween;
                i++;
            }

            return i;
        }

        int CreateSideBar_displayNoteUnselcted (Note note, int i) {
            int nameWidth = GetLengthInPixels(font, note.Name);

            Label lblTitle = new Label();
            lblTitle.Location = new Point(SIDE_MARGIN_LEFT, 5+29*i+sumHeight);
            lblTitle.Size = new Size(MinInt(nameWidth+5, 240-4), 24);
            lblTitle.Text = note.Name;
            lblTitle.Font = font;
            sideBar.Controls.Add(lblTitle);
            lblTitle.Click += selectNote;
            lblTitle.DoubleClick += selectNote;
            lblTitle.DoubleClick += editNote_Click;

            Label lblDescr = new Label();
            lblDescr.Location = new Point(SIDE_MARGIN_LEFT, 29+(93-64)*i+sumHeight);
            lblDescr.AutoSize = true;
            lblDescr.MaximumSize = new Size(240, 0);
            lblDescr.ForeColor = System.Drawing.Color.DarkGray;
            lblDescr.Text = note.Description;
            lblDescr.Font = fontSmall;
            sideBar.Controls.Add(lblDescr);
            lblDescr.DoubleClick += selectNote;
            lblDescr.DoubleClick += editNote_Click;

            void editNote_Click (object sender, EventArgs e) {
                CreateDialogAdd("Edit", note.Tag, note.Name, note.Description, note.ID);
            }

            void selectNote(object sender, EventArgs e) {
                noteIDSelected = note.ID;
                RefreshSidebar();
            }

            return lblDescr.Size.Height;
        }

        int CreateSideBar_displayNoteSelcted (Note note, int i) {
            int nameWidth = GetLengthInPixels(font, note.Name);

            Label lblTitle = new Label();
            lblTitle.Location = new Point(SIDE_MARGIN_LEFT, 5+29*i+sumHeight);
            lblTitle.Size = new Size(MinInt(nameWidth+5, 240-44*2-5), 24);
            lblTitle.BackColor = System.Drawing.Color.Black;
            lblTitle.ForeColor = System.Drawing.Color.White;
            lblTitle.Text = note.Name;
            lblTitle.Font = font;
            sideBar.Controls.Add(lblTitle);
            lblTitle.Click += selectNote;
            lblTitle.DoubleClick += selectNote;
            lblTitle.DoubleClick += editNote_Click;

            Button editNote = new Button();
            editNote.Location = new Point(SIDE_MARGIN_LEFT+240-44*2, 5+29*i+sumHeight);
            editNote.Size = new Size(40, 24);
            editNote.Text = "Edit";
            editNote.Font = fontSmall;
            editNote.DialogResult = DialogResult.OK;
            sideBar.Controls.Add(editNote);
            editNote.Click += editNote_Click;

            Button removeNote = new Button();
            removeNote.Location = new Point(SIDE_MARGIN_LEFT+240-44, 5+29*i+sumHeight);
            removeNote.Size = new Size(40, 24);
            removeNote.Text = "Del";
            removeNote.Font = fontSmall;
            removeNote.DialogResult = DialogResult.OK;
            sideBar.Controls.Add(removeNote);
            removeNote.Click += removeNote_Click;

            Label lblDescr = new Label();
            lblDescr.Location = new Point(SIDE_MARGIN_LEFT, 29+(93-64)*i+sumHeight);
            lblDescr.AutoSize = true;
            lblDescr.MaximumSize = new Size(240, 0);
            lblDescr.ForeColor = System.Drawing.Color.Black;
            lblDescr.Text = note.Description;
            lblDescr.Font = fontSmall;
            sideBar.Controls.Add(lblDescr);
            lblDescr.DoubleClick += selectNote;
            lblDescr.DoubleClick += editNote_Click;

            void removeNote_Click (object sender, EventArgs e) {
                calendar.RemoveNote(dateSelected.Date, note.ID);
            }

            void editNote_Click (object sender, EventArgs e) {
                CreateDialogAdd("Edit", note.Tag, note.Name, note.Description, note.ID);
            }

            void selectNote(object sender, EventArgs e) {
                noteIDSelected = note.ID;
                RefreshSidebar();
            }

            return lblDescr.Size.Height;
        }

        int CreateSideBar_contentNotes (int i) {
            foreach (Note note in calendar.GetNotes(dateSelected.Date)) {
                if (note.Name.Length == 0) {
                    continue;
                }

                int h = 0;

                if (noteIDSelected == note.ID)
                    h = CreateSideBar_displayNoteSelcted(note, i);
                else
                    h = CreateSideBar_displayNoteUnselcted(note, i);

                sumHeight += h;

                i++;
            }
            return i;
        }

        void CreateSideBar ()
        {
            if (calendar.Contains(dateSelected.Date))
            {
                int i = 0;
                sumHeight = 0;

                int notesNum = calendar.Length(dateSelected.Date);
                int lastTagLength = 0;
                int leftShift = 0;

                List<string> usedTags = new List<string>();

                i = CreateSideBar_contentTags(i, notesNum, lastTagLength, leftShift, usedTags);

                if (i>0)
                    sumHeight += 10 + 29;
                i = 0;

                i = CreateSideBar_contentNotes(i);
            }
        }

        // Graphics: calendar grid, date, calendar's controls
        void OnPaint_displayControls (Graphics g, StringFormat sf) {
            int d_x = D_X_CALENDAR_GRID;
            int d_y = D_Y_CALENDAR_GRID;
            int r = RADIUS;

            int x = MARGIN_LEFT + d_x / 2;
            int y = MARGIN_TOP - d_y * 2 - d_y / 2;
            Rectangle rectText = new Rectangle(x-r*2, y-r, r*2*8, r*3);
            string str = $"{dateSelected.DayOfWeek}, {dateSelected.ToString("MMMM")} {dateSelected.Day}, {dateSelected.Year}";
            g.DrawString ($"{str}", fontTitle, Brushes.Black, rectText, sf);

            x = MARGIN_LEFT + d_x * 2 + d_x / 2;
            y = MARGIN_TOP + d_y * 6;
            Rectangle rectSmall = new Rectangle(x-r, y-r, d_x, d_y);
            g.FillRectangle (Brushes.LightGray, rectSmall);
            g.DrawString ("<", fontTitle, Brushes.WhiteSmoke, rectSmall, sf);

            x = MARGIN_LEFT + d_x * 3 + d_x / 2;
            rectSmall = new Rectangle(x-r, y-r, d_x, d_y);
            g.FillRectangle (Brushes.LightGray, rectSmall);
            g.DrawString (">", fontTitle, Brushes.WhiteSmoke, rectSmall, sf);

            Point p1 = new Point(MARGIN_LEFT*2 + D_X_CALENDAR_GRID*6 - 1, 15);
            Point p2 = new Point(MARGIN_LEFT*2 + D_X_CALENDAR_GRID*6 - 1, 485);
            g.DrawLine(Pens.LightGray, p1, p2);

            Rectangle rectSmaller = new Rectangle(510, 15+SIDE_MARGIN_TOP_ADDING, 24, 24);
            g.FillRectangle (Brushes.LightGray, rectSmaller);
            g.DrawString ("+", fontTitle, Brushes.WhiteSmoke, rectSmaller, sf);

            rectText = new Rectangle(510+24, 15+SIDE_MARGIN_TOP_ADDING, 240, 24);
            g.DrawString ($"Tags & notes for {dateSelected.ToString("MMMM dd")}", font10B, Brushes.DarkGray, rectText, sf);
        }

        void OnPaint_displayDaysOfWeek (Graphics g, StringFormat sf) {
            int d_x = D_X_CALENDAR_GRID;
            int d_y = D_Y_CALENDAR_GRID;
            int r = RADIUS;

            int i = 0;

            foreach (DayOfWeek dayOfWeek in Enum.GetValues(typeof(DayOfWeek))) {
                int x = MARGIN_LEFT + d_x * i;
                int y = MARGIN_TOP - d_y;

                Rectangle rectSmall = new Rectangle(x-r, y-r, d_x, d_y);
                string str = dayOfWeek.ToString();
                g.DrawString ($"{str.Substring(0, 2)}", fontBold, Brushes.Black, rectSmall, sf);

                i++;
                i %= 7;
            }
        }

        void OnPaint_displayCalendar (Graphics g, StringFormat sf) {
            int d_x = D_X_CALENDAR_GRID;
            int d_y = D_Y_CALENDAR_GRID;
            int r = RADIUS;

            int i = 0;
            int j = 0;

            DateTime startFrom = Program.GetStart(dateSelected);
            bool selectedMonth = false;

            foreach (DateTime dt in Program.WholeMonthPlus(startFrom)) {
                int x = MARGIN_LEFT + d_x * i;
                int y = MARGIN_TOP + j / 7 * d_y;

                Rectangle rectSmall = new Rectangle(x-r, y-r, d_x, d_y);

                if (dt.Day == 1)
                    selectedMonth = !selectedMonth;

                if (selectedMonth) {
                    if (dt.Date == dateSelected.Date) {
                        g.FillRectangle (Brushes.Black, rectSmall);
                        g.DrawString ($"{dt.Day}", font, Brushes.WhiteSmoke, rectSmall, sf);
                    }
                    else if (noteIDSelected != -1 && calendar.IsDateTagged(dt.Date, dateSelected.Date, noteIDSelected)) {
                        g.FillRectangle (Brushes.Green, rectSmall);
                        g.DrawString ($"{dt.Day}", font, Brushes.WhiteSmoke, rectSmall, sf);
                    }
                    else
                        g.DrawString ($"{dt.Day}", font, Brushes.Black, rectSmall, sf);
                }
                else
                    g.DrawString ($"{dt.Day}", font, Brushes.Gray, rectSmall, sf);

                i++;
                i %= 7;
                j++;
            }
        }

        protected override void OnPaint(PaintEventArgs args) {
            Graphics g = args.Graphics;

            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            OnPaint_displayControls(g, sf);
            OnPaint_displayDaysOfWeek(g, sf);
            OnPaint_displayCalendar(g, sf);               
        }
    }
}
