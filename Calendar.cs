using System;
using System.Collections.Generic;
using static System.Console;
using System.IO;

namespace SemesterProj2
{
    class Note
    {
        public int ID;
        public string Tag;
        public string Name;
        public string Description;
        public DateTime Start;
        public DateTime End;


        public Note(int id, string Tag, DateTime Start, DateTime End, string Name = "", string Description = "") {
            this.ID = id;
            this.Tag = Tag;
            this.Name = Name;
            this.Description = Description;
            this.Start = Start;
            this.End = End;
        }


        public override string ToString() {
            return $"{this.ID}|{this.Tag}|{this.Name}|{this.Description}|{this.Start.ToString()}|{this.End.ToString()}";
        }


        static public Note FromString(string s) {
            string[] words = s.Trim().Split(null);
            Note note = new Note(int.Parse(words[0]), words[1], DateTime.Parse(words[4]), DateTime.Parse(words[5]), words[2], words[3]);
            return note;
        }
    }

    delegate void Notify();

    class Calendar
    {
        // Dictionary mapping all notes in the user's calendar to date
        Dictionary<DateTime, List<Note>> notes = new Dictionary<DateTime, List<Note>>();

        public Notify changed;  // event that fires whenever the calendar changes

        // Presupposition is, that the data is already prepared: Start's and End's date are the same
        public void AddNote(string Tag, DateTime Start, DateTime End, string Name = "", string Description = "") {
            DateTime dateOnly = Start.Date;
            int id = (notes.ContainsKey(dateOnly)) ? notes[dateOnly].Count : 0;
            Note note = new Note(id, Tag, Start, End, Name, Description);

            if (!notes.ContainsKey(dateOnly)) {
                List<Note> t = new List<Note>();
                t.Add(note);
                notes.Add(dateOnly, t);
            }
            else {
                notes[dateOnly].Add(note);
            }
            changed?.Invoke();
        }

        // Removes an object by date and id
        public void RemoveNote(DateTime date, int id) {
            notes[date.Date].RemoveAt(id);
            while (id < notes[date.Date].Count) {
                notes[date.Date][id].ID = id;
                id++;
            }
            changed?.Invoke();
        }


        public bool Contains(DateTime date) => notes.ContainsKey(date.Date);

        // All notes from a day
        public List<Note> GetNotes(DateTime date) => notes[date.Date];


        public int Length(DateTime date) => notes[date.Date].Count;


        const string defaultPath = "calen.dar";


        public void ReadFromDefault() {
            if (System.IO.File.Exists(defaultPath))
                ReadFromFile(defaultPath);
        }


        public void ReadFromFile(string filename) {
            string[] lines = File.ReadAllLines(filename);
            int i = 1;
            DateTime dt;
            string tag = "";
            string name = "";
            string descr = "";

            foreach (string line in lines) {
                if (i == 1) {
                    dt = DateTime.Parse(line);
                }
                else if (i == 0) {
                    i = int.Parse(line) + 2;
                }
                else {
                    //Note note = Note.fromString(line);
                    string[] words = line.Trim().Split("|");
                    if (words.Length == 4) {
                        tag = words[1];
                        name = words[2];
                        descr = words[3];
                        i++;
                    }
                    else if (words.Length == 1) {
                        descr += "\n" + words[0];
                        i++;
                    }
                    else if (words.Length == 6)
                        this.AddNote(words[1], DateTime.Parse(words[4]), DateTime.Parse(words[5]), words[2], words[3]);
                    else if (words.Length == 3)
                        this.AddNote(tag, DateTime.Parse(words[1]), DateTime.Parse(words[2]), name, descr+"\n"+words[0]);
                }
                i--;
            }
            changed?.Invoke();
        }


        public string GetTag(DateTime date, int id) {
            if (!notes.ContainsKey(date.Date))
                return "";
            if (id >= 0 && id < notes[date.Date].Count)
                return notes[date.Date][id].Tag;
            return "";
        }


        public bool IsDateTagged(DateTime date, DateTime sDate, int id) {
            if (!notes.ContainsKey(sDate.Date))
                return false;
            string tag = GetTag(sDate.Date, id);
            if (notes.ContainsKey(date.Date)) {
                foreach (Note note in notes[date.Date]) {
                    if (note.Tag == tag)
                        return true;
                }
            }
            return false;
        }
        

        public void WriteInDefault() {
            WriteInFile(defaultPath);
        }


        public void WriteInFile(string filename) {
            StreamWriter file = new StreamWriter(filename);

            foreach ((DateTime dt, List<Note> ls) in notes) {
                string s = $"{dt.ToString()}";
                file.WriteLine(s);
                s = $"{ls.Count}";
                file.WriteLine(s);

                foreach (Note note in ls) {
                    s = note.ToString();
                    file.WriteLine(s);
                }
            }

            file.Close();
        }

    }
}
