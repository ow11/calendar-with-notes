using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProj2
{
    static class Program
    {
        static public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for(var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        static public IEnumerable<DateTime> WholeMonthPlus(DateTime fromSunday)
        {
            DateTime thru = fromSunday.AddDays(41);
            for(var day = fromSunday.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        static public DateTime GetStart(DateTime chosen) {
            DateTime startFrom = new DateTime(chosen.Year, chosen.Month, 1);
            int delta = (int)startFrom.DayOfWeek % 7;
            startFrom = startFrom.AddDays(-delta);
            return startFrom;
        }

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
